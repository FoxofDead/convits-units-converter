﻿using Convits.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace Convits.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}